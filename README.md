# cfg

My configuration files for public consumption.

## Not My Actual Configs

These aren't direct copies of my actual configs, but files I've put online
because people asked for them or because I wanted to share them.

Generally they cannot be used as drop-in configs because entries might have
been redacted or removed.

My actual configs are on a synced (think: Syncthing) Git repo.  Putting
these online and having to think of redacting items and credentials is too
much of a hassle and something I'll forget to do sooner or later.

The hassle would also keep me from making commits for each change.

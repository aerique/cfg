# Guix

## MSI GS66 Stealth

### To Do

- [ ] hibernate

### Init

- put `dot-channels.scm` in `~/.config/guix/channels.scm`
- `guix pull`
- `sudo guix system reconfigure ~/cfg/system.scm`
- `guix home reconfigure ~/cfg/home.scm`

### Touchpad Middle Mouse Button

- Three-finger press in the middle of the pad functions as the middle mouse button.

## Nvidia current kernel params

- `cat /proc/driver/nvidia/params`

General debugging: https://askubuntu.com/questions/710172/why-do-xrandr-errors-badmatch-badname-gamma-failed-happen/727897#727897

https://wiki.archlinux.org/title/NVIDIA/Troubleshooting
https://wiki.archlinux.org/title/NVIDIA#DRM_kernel_mode_setting
https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Screen_corruption_after_resuming_from_suspend
https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Screen_corruption_after_resuming_from_suspend
https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Overclocking_with_nvidia-settings_GUI_not_working
https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Blackscreen_at_X_startup_/_Machine_poweroff_at_X_shutdown

# Nvidia & Green With Envy

Coolbits need to be set (see `work-laptop-msi.scm`, X needs to run as root
(so log in as root) and `xhost si:localuser:root ` needs to be set.

```
; https://gist.github.com/greigdp/bb70fbc331a0aaf447c2d38eacb85b8f#sleep-mode-power-usage
;"mem_sleep_default=deep"
```

Above (`deep` enabled) since to give slighlty lower power draw.
(Like 0.1W lower, 1.4 vs 1.5)

- https://wiki.archlinux.org/title/NVIDIA/Tips_and_tricks#Set_fan_speed_at_login
- https://forums.developer.nvidia.com/t/how-to-limit-power-consumption-on-495-46/199945
- https://wiki.archlinux.org/title/MSI_GS66_11UX
- https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Carbon_(Gen_7)#Freeze_when_suspending
- https://guix.gnu.org/manual/en/html_node/Power-Management-Services.html
- https://superuser.com/questions/861530/what-does-sink-output-source-output-sink-offload-source-offload-mean-for-gp
- https://www.kernel.org/doc/html/latest/power/basic-pm-debugging.html

https://github.com/YoyPa/isw
- https://github.com/Askannz/msi-perkeyrgb
- https://wiki.archlinux.org/title/MSI_GS66_11UX
- https://github.com/YoCodingMonster/OpenFreezeCenter
- https://old.reddit.com/r/VFIO/wiki/index

```
> vulkaninfo ^C
> glxgears -info ^C
> glxinfo ^C
> glxheads ^C
> glxdemo ^C
> glxgears -info | grep GL_RENDERER ^C
> glxinfo | grep -i opengl ^C
```

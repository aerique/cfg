;; home.scm
;;
;; To reproduce the exact same profile, you also need to capture the
;; channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu home services)
             (gnu home services mcron)
             (gnu home services shells)
             (gnu packages)
             (gnu packages gnome)
             (gnu services)
             (guix gexp))

(use-modules (aerique packages sync))

(home-environment
  (packages
    (map (compose list specification->package+output)
         (list "ascii" "asciinema" "autokey" "autorandr" "bat" "bind"
               "cheese" "colordiff" "colormake" "dpkg" "dunst"
               ;; Doom Emacs needs Emacs 28.
               "emacs@28"
               "entr" "expect" "fd" "feh" "ffmpeg" "fish" "fuse" "fzf"
               ;; Firefox is `guix install`ed since I do not want to
               ;; wait on a 6 hour build when doing a quick reconfigure.
               ;"firefox"
               "geeqie" "gimp" "git" "gnupg" "gnuplot" "gramps"
               ;; Same reason as for Firefox:
               ; "google-chrome-stable"
               "gpick"
               "groff-minimal"  ; for Docker aws-cli
               "hicolor-icon-theme" "htop" "imagemagick" "jotta-cli" "jc" "jq"
               "keepassxc" "libreoffice" "lm-sensors" "libnotify" "ltrace"
               "lxappearance" "make" "makepasswd" "markdown" "meld" ;"minikube"
               "moc" "mpv" "mupdf"
               "ncdu" "neofetch" "nmap" "nftables"
               "openssl" "openvpn" "patchelf" "pavucontrol" "pinentry-tty"
               "powerstat" "powertop" "qbittorrent" "rclone" "recordmydesktop"
               "recutils" "ripgrep" "rlwrap" "rofi" "rxvt-unicode" "sbcl"
               "signal-desktop"
               "signify" "sqlite" "sshfs" "sshuttle" "strace" "syncthing"
               "tig" "toot" "tree" "unzip" "volumeicon"
               "woof"
               "xautomation" "xclip" "xdg-user-dirs" "xdg-utils" "xdotool"
               "xfontsel" "xhost" "xmodmap" "xprop" "xwininfo"
               ;; Use `guix install` for these: they need to be updated often
               ;"youtube-dl" "yt-dlp"
               "zip"
               ;; Android
               "adb" "fastboot"
               ;; Doom Emacs
               "aspell" "aspell-dict-en" "aspell-dict-nl" "cmake"
               "gcc-toolchain" "node" "python-isort" "python-nose"
               "python-pytest" "python-wrapper" "rtags" "shellcheck"
               "tidy-html"
               ;; Games
               "steam" "steam-nvidia"
               ;; Flatpak for Zoom.
               "flatpak"
               ;; i3blocks
               "alsa-utils" "perl" "sysstat")))
  (services
   (list
    (service home-bash-service-type
             (home-bash-configuration
              (guix-defaults? #t)
              (aliases
               '(("b" . "bat")
                 ("c" . "clear")
                 ("cat" . "bat --style=plain")
                 ("diff" . "colordiff")
                 ("dmesg" . "sudo dmesg")
                 ("flatpak" . "flatpak --user")
                 ("l" . "ls -lhp --group-directories-first --color=auto")
                 ("mocp" . "mocp --theme aerique_theme")
                 ("mupdf" . "mupdf-x11")
                 ;; Flatpak Aliases
                 ;("chrome" . "flatpak --user run com.google.Chrome")
                 ("zoom" . "flatpak --user run us.zoom.Zoom")))
              (environment-variables
               '(("GUIX_LOCPATH" . "${HOME}/.guix-profile/lib/locale")
                 ("PATH" . "${PATH}:${HOME}/.local/bin")
                 ("TERM" . "xterm")
                 ("XDG_DATA_DIRS" . "${XDG_DATA_DIRS}:/var/lib/flatpak/exports/share:${HOME}/.local/share/flatpak/exports/share")))
              ;; I'd rather just list strings, but this'll do for now.
              (bash-profile
               `(,(plain-file "bash-profile"
                              "eval \"$(dircolors -b ${HOME}/.dircolors)\""))))))))
    ;(service home-mcron-service-type
    ;         (home-mcron-configuration
    ;          (jobs (list ;#~(job '(next-minute) "notify-send lul")
    ;                 )))))))

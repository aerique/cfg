;;;; system.scm

;;; Modules

(use-modules (gnu)
             #|(guix transformations)|#)
(use-package-modules bash cups firmware fonts gnome rsync ssh wm xorg)
(use-service-modules cups desktop docker networking ssh xorg #|virtualization|#)

(use-modules (nongnu packages linux)
             (nongnu packages nvidia)
             (nongnu system linux-initrd))

(use-modules (aerique packages finance))

;; Example to show what's in a service.
;(use-modules (srfi srfi-1))
;(format #t "base-services:~%")
;(do ((bs %base-services (cdr bs)))
;    ((null? bs))
;  (format #t "  - ~a~%" (first bs)))


;;; Functions

(define etc-hosts-config
  (plain-file "etc-hosts-config"
              "«REDACTED»"))


(define etc-resolv-config
  (plain-file "etc-resolv-config"
              "«REDACTED»"))

;; https://unix.stackexchange.com/questions/201858/what-does-all-all-all-all-mean-in-sudoers
;; The first ALL is the users allowed
;; The second one is the hosts
;; The third one is the user as you are running the command
;; The last one is the commands allowed
(define etc-sudoers-config
  (plain-file "etc-sudoers-config"
              "Defaults   timestamp_timeout=480
root       ALL=(ALL) ALL
%wheel     ALL=(ALL) ALL
«REDACTED» ALL=(ALL) NOPASSWD:/run/current-system/profile/bin/chvt,/run/current-system/profile/bin/loginctl"))


;; Flipper Zero
(define %flipper-zero-udev-rules
  (udev-rule
    "42-flipperzero.rules"
    (string-append "# Flipper Zero serial port
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", ATTRS{idProduct}==\"5740\", ATTRS{manufacturer}==\"Flipper Devices Inc.\", TAG+=\"uaccess\", GROUP=\"dialout\"
# Flipper Zero DFU
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", ATTRS{idProduct}==\"df11\", ATTRS{manufacturer}==\"STMicroelectronics\", TAG+=\"uaccess\", GROUP=\"dialout\"
# Flipper ESP32s2 BlackMagic
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"303a\", ATTRS{idProduct}==\"40??\", ATTRS{manufacturer}==\"Flipper Devices Inc.\", TAG+=\"uaccess\", GROUP=\"dialout\"")))


;(define transform
;  (options->transformation '((with-graft . "mesa=nvda"))))


;;; Operating System definition (this needs to come last)

(operating-system
  ;(kernel linux)      ; Nvidia 470 driver doesn't build here.
  ;(kernel linux-lts)
  (kernel linux-5.15)  ; linux-5.15 for the working Nvidia 470 driver.
  (kernel-loadable-modules (list nvidia-module))
  ;; Blacklisting Nouveau so Intel i915 will be used (works better atm).
  (kernel-arguments
    (append (list (string-append "modprobe.blacklist=bluetooth,btrtl,btintel,"
                                 "btcm,btusb,nouveau,pcspkr")
                  "cpufreq.default_governor=powersave"
                  "mem_sleep_default=deep"  ; slightly lower power draw
                  "nvidia.NVreg_RegistryDwords=\"OverrideMaxPerf=0x0\"")
                  ;; Docker "cannot find cgroup mount destination: unknown"
                  ;"systemd.unified_cgroup_hierarchy=false")
            %default-kernel-arguments))
  (initrd microcode-initrd)
  (firmware (list linux-firmware sof-firmware))
  (locale "en_DK.utf8")
  (timezone "Europe/Amsterdam")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "«REDACTED»")
  (hosts-file etc-hosts-config)
  (sudoers-file etc-sudoers-config)
  (users (cons* (user-account
                  (name "«REDACTED»")
                  (comment "«REDACTED»")
                  (group "users")
                  (home-directory "/home/«REDACTED»")
                  (supplementary-groups '("wheel" "netdev" "audio" "video" "dialout"
                                          "docker" "kvm" #|"libvirt"|#)))
                %base-user-accounts))
  ;; Install `gnupg` as a user so Flatpak fuckery will be detected.
  ;;
  ;; Most packages should be installed by the user. The packages that end up
  ;; here are packages we want to be available in a bare system (i.e. for
  ;; recovery) *or* because they cause issues when installed as a normal user.
  ;;
  ;; (The reverse also applies, some packages cause issues when installed as a
  ;; sytem package. (No examples at the moment.))
  ;;
  ;; - flameshot: acted weird when installed through home profile
  ;;
  ;; Move to user?: feh
  (packages (append
             (map specification->package
                  '("brightnessctl" "cpupower" "curl" "dmenu" "docker-compose"
                    "dxvk" "file" "flameshot" "font-dejavu" "font-terminus"
                    "fontconfig" "glibc-locales"
                    "i3-wm" "i3blocks" "i3lock" "i3status"
                    "inxi" "lshw"
                    "lsof" "mc" "mesa-utils" "nss-certs" ;"ovmf"
                    "rsync"
                    "vulkan-tools" "w3m" "xauth" "xdpyinfo" "xev" "xkill"
                    "xrandr" "xset" "xterm" "xz"
                    "ncurses"    ; for `clear`
                    "vim-full")) ; for `+clipboard`
             %base-packages))
  (services
    ;; Brother DCP-9020CDW:
    ;; - ipp://192.168.178.67/ipp/port1 ("Generic IPP Everywhere Printer")
    ;; - "Generic PCL 6/PCL XL Printer - CUPS+Gutenprint v5.3.3" (old laptop)
    (append (list (service cups-service-type
                           (cups-configuration
                            (web-interface? #t)
                            (extensions (list cups-filters brlaser
                                              foomatic-filters))))
                  (service docker-service-type)
                  ;(service libvirt-service-type)
                  ;(service virtlog-service-type)
                  (service openssh-service-type
                    (openssh-configuration (password-authentication? #false)
                                           (port-number 22)))
                  (pam-limits-service
                    ;; For Lutris / Wine esync
                    (list (pam-limits-entry "*" 'hard 'nofile 524288)))
                  (screen-locker-service i3lock "i3lock")
                  (simple-service 'resolv.conf etc-service-type
                                  (list `("resolv.conf" ,etc-resolv-config)))
                  (udev-rules-service 'flipper-zero %flipper-zero-udev-rules)
                  (udev-rules-service 'ledger-devices ledger-udev-rules)
                  (udev-rules-service 'nvidia-gpu nvidia-driver)
                  (extra-special-file "/bin/bash" (file-append bash "/bin/bash"))
                  ;; Might hopefully make running 3rd-party binaries easier,
                  ;; but might also break stuff. (Things that `conda` installs,
                  ;; or binaries in an AppImage.)
                  (extra-special-file "/lib64/ld-linux-x86-64.so.2"
                               (file-append glibc "/lib/ld-linux-x86-64.so.2"))
                  ;; `rsync` and `ssh` are so Ansible won't shit itself
                  ;; This can't go into a `guix shell` manifest since Ansible
                  ;; checks for hardcoded paths.
                  (extra-special-file "/usr/bin/rsync"
                                      (file-append rsync "/bin/rsync"))
                  (extra-special-file "/usr/bin/ssh"
                                      (file-append openssh "/bin/ssh"))
                  ;(extra-special-file "/usr/share/OVMF/OVMF_CODE.fd"
                  ;           (file-append ovmf "/share/firmware/ovmf_x64.bin"))
                  ;(extra-special-file "/usr/share/OVMF/OVMF_VARS.fd"
                  ;                    "/home/brainrot/vms/OVMF_VARS.fd")
                  (set-xorg-configuration
                    (xorg-configuration
                      (modules (cons* nvidia-driver %default-xorg-modules))
                      ;(server (transform xorg-server))
                      ;; If `nvidia` comes second, the desktop on the external
                      ;; screen will be slow as ass. (Browsers mainly.)
                      ;(drivers '("nvidia" "modesetting"))
                      ;; Trying this for a while since starting slow apps with
                      ;; `DRI_PRIME=1` works.
                      (drivers '("modesetting" "nvidia"))
                      ;(drivers '("modesetting"))
                      (keyboard-layout keyboard-layout))))
            ;%desktop-services))
            (modify-services %desktop-services
              (delete bluetooth-service)
              ;(delete pulseaudio-service-type)  ; someday, please god
              (console-font-service-type config =>
                (map (lambda (tty)
                       ;(cons tty %default-console-font))
                       (cons tty (file-append font-terminus
                                              "/share/consolefonts/ter-u32n")))
                     '("tty1" "tty2" "tty3" "tty4" "tty5" "tty6")))
              (guix-service-type config => (guix-configuration
                (inherit config)
                (substitute-urls (append '("https://substitutes.nonguix.org")
                                         %default-substitute-urls))
                (authorized-keys (append (list (local-file "signing-key.pub"))
                                         %default-authorized-guix-keys))))
              (elogind-service-type config => (elogind-configuration
                (inherit config)
                (handle-lid-switch-external-power 'suspend)))
              ;; FIXME this doesn't work yet
              (network-manager-service-type config => (network-manager-configuration
                (inherit config)
                (vpn-plugins (list network-manager-openvpn)))))))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (theme (grub-theme (inherit (grub-theme))
                         (gfxmode '("1600x1200" "auto"))))
      (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device (source (uuid "«REDACTED»"))
                         (target "cryptroot")
                         (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "ext4")
             (dependencies mapped-devices))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "«REDACTED»" 'fat32))
             (type "vfat"))
           %base-file-systems)))
